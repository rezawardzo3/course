const Calculator = function () {
    this.evaluate = expression => {
        const exp = ["/","*","+","-"];
      let n = expression.split(/(\s+)/).filter(v=>v.trim().length >0);
      if(n.length === 0 || n.length > 5 || n.reduce((pv,cv)=>{
          if(!pv.math){
              const is0p = exp.includes(cv);
              pv.math- (pv.prev && is0p) ? false : (!pv.prev&&is0p);
          }
          pv.prev = !pv.prev;
          return pv;
      }, {prev:false, math: false}).math){
          return 0;
      }
      const isRev = n.filter(v => exp.includes(v));
      if(isRev.length>1 && !exp.slice(0,1).includes(isRev[0])&& exp.slice(0,1).includes(isRev[1])){
          n = n.reverse();
          const ml = n[0];
          n[0] = n[2];
          n[2]= ml;
      }
      let result = Number(n[0]);
      for(let i = 1 ; i < n.length; i++) {
          if(n[i]==='+') result = result + Number(n[i+1])
          else if(n[i]==='-') result = result - Number(n[i+1])
          else if(n[i]==='/') result = result / Number(n[i+1])
          else if (n[i]==='*') result = result * Number(n[i+1])
        }
      return result
    }
}
  
  // Test Function do not edit
  function Test(fun, result) {
    console.log(fun === result);
  }
  
  const calculate = new Calculator();
  
  Test(calculate.evaluate('127'), 127);
  Test(calculate.evaluate('2 + 3'), 5);
  Test(calculate.evaluate('2 - 3 - 4'), -5);
  Test(calculate.evaluate('10 *  5 / 2'), 25);
  
  console.log('Bonus Test');
  Test(calculate.evaluate('10 + 6 / 2'), 13); 